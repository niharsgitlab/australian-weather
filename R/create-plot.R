# Simple script analysing the Australian weather data

library("tidyverse")

# Read in the data sdf

df = read_csv("data/weatherAUS.csv")

# Create a scatter plot of Humidity9am and Temp9am
# a location

city = "Sydney"

scatter_plot =
  df %>%
  filter(Location == "Brisbane") %>%
  ggplot(aes(x = Temp9am, y = Humidity9am)) +
  geom_point(colour = "red") +
  labs(title = "Humidity and Temperatur for Brisbane") +
  theme_minimal()

# Save the plot in graphics/
ggsave(filename = "graphics/scatter-temp-humidity.png", plot = scatter_plot)

time_series =
  df %>%
  filter(Location == city) %>%
  ggplot(aes(x = Date, y = Temp9am)) +
  geom_line() + geom_point(colour = "green")
ggsave(filename = "graphics/timeseries-rainfall.png",
       plot = scatter_plot)

ggsave(filename = "graphics/timeseries_Rain.png", plot = scatter_plot)
